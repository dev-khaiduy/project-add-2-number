import { createLogger, transports } from 'winston';

const logLevels = {
  fatal: 0,
  error: 1,
  warn: 2,
  info: 3,
  debug: 4,
  trace: 5,
};

const logger = createLogger({
  levels: logLevels,
  transports: [new transports.Console()],
});

export default class MyBigNumber {
  static sum(stn1, stn2) {
    let result = ''; // Khởi tạo kết quả ban đầu là chuỗi rỗng
    let carry = 0; // Khởi tạo biến nhớ ban đầu là 0
    let i = stn1.length - 1; // Khởi tạo chỉ số bắt đầu từ cuối chuỗi stn1
    let j = stn2.length - 1; // Khởi tạo chỉ số bắt đầu từ cuối chuỗi stn2

    logger.info(`Summing ${stn1} and ${stn2}...`); // Ghi lại thông tin về phép toán

    // Duyệt đồng thời chuỗi stn1 và stn2 từ phải sang trái và cộng từng kí số
    while (i >= 0 || j >= 0) {
      let digit1 = i >= 0 ? parseInt(stn1.charAt(i)) : 0; // Lấy kí số ở vị trí i hoặc là 0 nếu i < 0
      let digit2 = j >= 0 ? parseInt(stn2.charAt(j)) : 0; // Lấy kí số ở vị trí j hoặc là 0 nếu j < 0
      let sum = digit1 + digit2 + carry; // Cộng kí số ở vị trí i và j cùng với số nhớ

      logger.info(`${digit1} + ${digit2} + ${carry} = ${sum}`); // Ghi lại thông tin về phép toán

      result = (sum % 10) + result; // Lưu kết quả vào chuỗi kết quả
      carry = sum >= 10 ? 1 : 0; // Nếu tổng lớn hơn hoặc bằng 10, ghi nhớ 1, ngược lại ghi nhớ 0
      i--; // Giảm chỉ số i đi 1
      j--; // Giảm chỉ số j đi 1
    }

    // Nếu còn số nhớ, thêm số nhớ vào đầu chuỗi kết quả
    if (carry > 0) {
      result = carry + result;
      logger.info(`Carry ${carry} is added to the result`);
    }

    logger.info(`The result is ${result}`); // Ghi lại thông tin về kết quả

    return result; // Trả về chuỗi kết quả
  }
}

//Test code

let bigNumber = MyBigNumber.sum("1234", "5678");
console.log(bigNumber);

