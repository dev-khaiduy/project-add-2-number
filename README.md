# MyBigNumber

Lớp MyBigNumber được sử dụng để cộng hai số lớn bằng cách thực hiện phép cộng từng chữ số.

## Yêu cầu

Lớp MyBigNumber được viết bằng JavaScript, vì vậy bạn cần cài đặt Node.js trước khi sử dụng lớp này.

# Khởi chạy 

**Các bước thực hiện**

# 1. Tải mã nguồn từ Github:
git clone https://gitlab.com/dev-khaiduy/project-add-2-number.git

# 2. Cài đặt các thư viện cần thiết:
npm install

# 3. Khởi động project:
npm start


